package co.golky.app.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;

@Service
public class CropImageService {

	public File crop(File filePath, int x, int y, int w, int h){
		try {
			BufferedImage originalImgage = ImageIO.read(filePath);
	        BufferedImage subImgage = originalImgage.getSubimage(x, y, w, h);  
	        
	        String extension = FilenameUtils.getExtension(filePath.getName());
	        
	        UUID uuid = UUID.randomUUID();
	        File outputfile = File.createTempFile(uuid.toString(), uuid.toString()+"."+extension);
			ImageIO.write(subImgage, extension, outputfile); 
	        
	        return outputfile;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
