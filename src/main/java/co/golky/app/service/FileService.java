package co.golky.app.service;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;

import co.golky.app.dto.Response;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class FileService {

	@Autowired
	private AwsS3Service awsS3Service;
	
	private final String folder = "ocr-test/";
	
	public Mono<Response> uploadFile(FilePart uploadFile) {
		try {
			if (uploadFile == null)
				return Mono.just(Response.builder().message("FILE_IS_REQUIRED").build());

			return this.getFileFromFilePart(uploadFile)
					.flatMap(file-> this.uploadFile(file));

		} catch (Exception e) {
			e.printStackTrace();
			return Mono.just(Response.builder().status(false).message(e.getMessage()).build());
		}
	}
	
	public Mono<Response> uploadFile(File file) {
		System.out.println("file: "+file);

		String extension = FilenameUtils.getExtension(file.getName());

		// se guarda el archivo
		String myUUID = UUID.randomUUID().toString();
		String nameFile = myUUID + "." + extension;
		String url = this.awsS3Service.saveFileInBucket(file, folder + nameFile);
		log.info("URL AWS: " + url);

		Map<String, Object> data = new HashMap<>();
		data.put("url", url);
		data.put("path", folder + nameFile);
			
		if (url != null)
			return Mono.just(Response.builder().status(true).message("FILE_SAVED").data(data).build());
		
		return Mono.just(Response.error("BAD_FILE"));
	}
	
	public Mono<File> getFileFromFilePart(FilePart file) throws IOException {
		File fileTemp = File.createTempFile("temp", file.filename());
		return file.transferTo(fileTemp).then(Mono.just(fileTemp));
	}

}
