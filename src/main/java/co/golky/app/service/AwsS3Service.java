package co.golky.app.service;

import java.io.File;
import java.net.URL;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

@Service
public class AwsS3Service {

	@Value("${variables.aws-bucket.name}") private String NAME_BUCKET;
	private AmazonS3 s3Client;

	public AwsS3Service(@Value("${variables.aws-credentials.ACCESS_KEY}") String ACCESS_KEY,
			@Value("${variables.aws-credentials.SECRET_KEY}") String SECRET_KEY) {
		
		AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_1).build();
		this.s3Client = s3Client;
		
	}
	
	public String saveFileInBucket(File file, String pathInBucket) {
		
		System.out.println(file.getTotalSpace()+ " - "+pathInBucket);

		PutObjectRequest request = new PutObjectRequest(NAME_BUCKET, pathInBucket, file);
		request.withCannedAcl(CannedAccessControlList.PublicRead);
		
		s3Client.putObject(request);
		URL url = s3Client.getUrl(NAME_BUCKET, pathInBucket);
		return url.toString();
	}
}
