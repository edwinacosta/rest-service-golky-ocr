package co.golky.app.service;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import co.golky.app.dto.PlayerData;
import co.golky.app.dto.Response;
import co.golky.app.dto.UploadFileDTO;
import co.golky.app.dto.UploadFileDTOWithCors;
import co.golky.app.textract.AnalyzeDocument;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Slf4j
@Service
public class GeneralService {
	
	@Autowired private FileService fileService;
	@Autowired private CropImageService cropImageService;
	
	@Value("${variables.aws-bucket.name}") private String NAME_BUCKET;
	@Value("${variables.aws-credentials.ACCESS_KEY}") String ACCESS_KEY;
	@Value("${variables.aws-credentials.SECRET_KEY}") String SECRET_KEY;

	public Mono<Response> analyze(UploadFileDTO uploadFileDTO) {

		return this.fileService.uploadFile(uploadFileDTO.getFile())
				.flatMap(res->{

					if(!res.isStatus()) return Mono.just(res);

					Map<String, Object> data = new HashMap<>();
					
					String path = res.getDataAsJsonObject().getString("path");
					String url = res.getDataAsJsonObject().getString("url");
					data.put("url", url);
					
					log.info("Start analyzing...");
					List<PlayerData> result = AnalyzeDocument.start(path, NAME_BUCKET, ACCESS_KEY, SECRET_KEY);
					
					data.put("result", result);
					
					return Mono.just(Response.success("OK", data));
				}).onErrorResume(e->{
					e.printStackTrace();
					return Mono.just(Response.builder().message("ERROR").build());
				});
	}

	public Mono<Response> analyzeWithCors(UploadFileDTOWithCors uploadFileDTOWithCors) {

		try {
			return this.fileService.getFileFromFilePart(uploadFileDTOWithCors.getFile())
					.flatMap(file->{
						
						File fileCropped1 = this.cropImageService
								.crop(file, uploadFileDTOWithCors.getTable1X(), uploadFileDTOWithCors.getTable1Y(), uploadFileDTOWithCors.getTable1Width(), uploadFileDTOWithCors.getTable1Height());

						
						File fileCropped2 = this.cropImageService
								.crop(file, uploadFileDTOWithCors.getTable2X(), uploadFileDTOWithCors.getTable2Y(), uploadFileDTOWithCors.getTable2Width(), uploadFileDTOWithCors.getTable2Height());


						Mono<Response> mono1 = this.fileService.uploadFile(fileCropped1)
								.subscribeOn(Schedulers.boundedElastic());
						Mono<Response> mono2 = this.fileService.uploadFile(fileCropped2)
								.subscribeOn(Schedulers.boundedElastic());
						
						return Mono.zip(mono1, mono2)
							.flatMap(tuple->{
								
								if(!tuple.getT1().isStatus()) return Mono.just(tuple.getT1());
								if(!tuple.getT2().isStatus()) return Mono.just(tuple.getT2());

								Map<String, Object> data = new HashMap<>();
								
								// team 1
								String path1 = tuple.getT1().getDataAsJsonObject().getString("path");
								String url1 = tuple.getT1().getDataAsJsonObject().getString("url");
								data.put("url1", url1);
								
								Mono<List<PlayerData>> mono3 = AnalyzeDocument.startAsync(path1, NAME_BUCKET, ACCESS_KEY, SECRET_KEY)
										.subscribeOn(Schedulers.boundedElastic());
								
								// team 2
								String path2 = tuple.getT2().getDataAsJsonObject().getString("path");
								String url2 = tuple.getT2().getDataAsJsonObject().getString("url");
								data.put("url2", url2);
								
								Mono<List<PlayerData>> mono4 = AnalyzeDocument.startAsync(path2, NAME_BUCKET, ACCESS_KEY, SECRET_KEY)
										.subscribeOn(Schedulers.boundedElastic());
								
								return Mono.zip(mono3, mono4)
										.flatMap(tuple2->{
											data.put("result2", tuple2.getT1());
											data.put("result1", tuple2.getT2());
											return Mono.just(Response.success("OK", data));
										});
							});
						
					});
		} catch (Exception e) {
			e.printStackTrace();
			return Mono.just(Response.error(e.getMessage()));
		}
		
	}

}
