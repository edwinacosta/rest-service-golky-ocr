package co.golky.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import co.golky.app.dto.Response;
import co.golky.app.dto.UploadFileDTO;
import co.golky.app.dto.UploadFileDTOWithCors;
import co.golky.app.service.GeneralService;
import co.golky.app.util.LogTransaction;
import reactor.core.publisher.Mono;

@RestController
public class GeneralController {
	
	@Autowired private GeneralService generalService;
	@Autowired private LogTransaction log;

	@PostMapping("/analyze")
	public Mono<Response> analyze(UploadFileDTO uploadFileDTO) {
		log.startTransaction("analyze", uploadFileDTO);
		return this.generalService.analyze(uploadFileDTO).doOnSuccess(log::endTransaction);		
	}
	
	@PostMapping("/analyze-with-cors")
	public Mono<Response> analyze(UploadFileDTOWithCors uploadFileDTOWithCors) {
		log.startTransaction("analyze-with-cors", uploadFileDTOWithCors);
		return this.generalService.analyzeWithCors(uploadFileDTOWithCors).doOnSuccess(log::endTransaction);		
	}
}
