package co.golky.app.util;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;

import co.golky.app.dto.Response;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class LogTransaction {
	
	@Value("${conf.log.long-line}") private int longLine;

	private String uuidOperation;
	private Date start;
	private final String spr = " - ";
	
	public void startTransaction(String requestType, Object request) {
		
		this.uuidOperation = UUID.randomUUID().toString().replace("-", "");
		this.start = new Date();
		
		log.info("\n\n");
		log.info("======================================================================================");
		log.info("[{}] TRANSACTION STARTED", uuidOperation);
		log.info("[{}] REST REQUEST  - Type  [{}]", uuidOperation, requestType);
		log.info("[{}] REST REQUEST  - Value [{}]", uuidOperation, toStringObject(request));
	}

	private Object toStringObject(Object request) {
		if(request == null)
			return "NULL";
		if(request.toString().length() > longLine)
			return request.toString().substring(0, longLine)+" - + "+request.toString().length()+" chars";
		return request.toString();
	}

	public void endTransaction(Response response) {
		log.info("[{}] REST RESPONSE - Value [{}]", uuidOperation, toStringObject(response));
	    log.info("[{}] TRANSACTION FINISHED", uuidOperation);
	    log.info("[{}] TRANSACTION DURATION - {} milliseconds ", uuidOperation, new Date().getTime()-this.start.getTime());
	    log.info("======================================================================================");
	}

	public String concat(Object item, Object item2) {
		return this.toString(item)+spr+this.toString(item2);
	}
	public String concat(Object item, Object item2, Object item3) {
		return this.toString(item)+spr+this.toString(item2)+spr+this.toString(item3);
	}
	public String concat(Object item, Object item2, Object item3, Object item4) {
		return this.toString(item)+spr+this.toString(item2)+spr+this.toString(item3)+spr+this.toString(item4);
	}
	public String concat(Object item, Object item2, Object item3, Object item4, Object item5) {
		return this.toString(item)+spr+this.toString(item2)+spr+this.toString(item3)+spr+this.toString(item4)+spr+this.toString(item5);
	}
	
	private String toString(Object item) {
		if(item == null)
			return "NULL";
		if(item instanceof FilePart)
			return ((FilePart) item).filename();
		return item.toString();
	}
	
	public void msj(String msj) {
		log.info("[{}] MSJ = [{}]", uuidOperation, msj);
	}
	
}
