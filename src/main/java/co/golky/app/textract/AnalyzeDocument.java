package co.golky.app.textract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.amazonaws.services.textract.AmazonTextract;
import com.amazonaws.services.textract.AmazonTextractClientBuilder;
import com.amazonaws.services.textract.model.AnalyzeDocumentRequest;
import com.amazonaws.services.textract.model.AnalyzeDocumentResult;
import com.amazonaws.services.textract.model.Block;
import com.amazonaws.services.textract.model.BlockType;
import com.amazonaws.services.textract.model.Document;

import com.amazonaws.services.textract.model.Relationship;
import com.amazonaws.services.textract.model.RelationshipType;
import com.amazonaws.services.textract.model.S3Object;

import co.golky.app.dto.PlayerData;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;

@Slf4j
public class AnalyzeDocument {
	
	private final static boolean debug = true;

	private static final List<String> titles = Arrays.asList("n°", "nombres e-1", "nombres e-2", "t.d.", "documento",
			"jugo", "a", "az", "r", "g");

	
	public static Mono<List<PlayerData>> startAsync(String document, String bucket, String ACCESS_KEY, String SECRET_KEY) {
		return Mono.just("")
				.map(empty-> start(document, bucket, ACCESS_KEY, SECRET_KEY));
	}
	
	public static List<PlayerData> start(String document, String bucket, String ACCESS_KEY, String SECRET_KEY) {
		log.info("Start analyzing...");
		Date start = new Date();

		// The S3 bucket and document
		// String document = "match10_page-0001.jpg"; new size
		// String document = "1615658206863-min.jpg";
		// String bucket =
		// "textract-console-us-east-1-a30aa65f-ac31-43cf-8dfa-49055bc617ab";

		AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);

		// Call AnalyzeDocument
		EndpointConfiguration endpoint = new EndpointConfiguration("https://textract.us-east-1.amazonaws.com",
				"us-east-1");

		AmazonTextract client = AmazonTextractClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).withEndpointConfiguration(endpoint)
				.build();

		AnalyzeDocumentRequest request = new AnalyzeDocumentRequest().withFeatureTypes("TABLES")
				.withDocument(new Document().withS3Object(new S3Object().withName(document).withBucket(bucket)));

		AnalyzeDocumentResult result = client.analyzeDocument(request);

		List<Block> tables = new ArrayList<>();
		Map<String, Block> blockMap = new HashMap<>();

		for (Block block : result.getBlocks()) {
			// printBlock(block);
			blockMap.put(block.getId(), block);
			if (block.getBlockType().equals("TABLE"))
				tables.add(block);
		}

		System.out.println("tables size: " + tables.size());

		List<PlayerData> allPlayers = new ArrayList<>();

		for (Block b : tables) {
			TableModel table = generateTable(b, blockMap);
			allPlayers.addAll(mapTable(table));
		}

		System.out.println(((new Date().getTime() - start.getTime()) / 1000.0) + " seconds");
		return allPlayers;

	}
	
	
	/*
	private static class ImageSize{
		int width = 0;
		int height = 0;
		
		public ImageSize(int _width, int _height) {
			width = _width;
			height = _height;
		}

		@Override
		public String toString() {
			return "ImageSize [width=" + width + ", height=" + height + "]";
		}
		
		
	}
	
	public static ImageSize getImageSize(File fileToWrite) {
		try {
			BufferedImage originalImgage = ImageIO.read(fileToWrite);
			return new ImageSize(originalImgage.getWidth(), originalImgage.getHeight());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ImageSize(0 ,0);
	}

	private static BoundingBox getCorsTeam1(AnalyzeDocumentResult result, String nameTeam) {
		if(nameTeam == null || nameTeam.isBlank() || nameTeam.isEmpty()) return null;
		for (Block block : result.getBlocks()) {
			if (block != null && block.getText() != null) {
				String text = block.getText().trim();
				if(nameTeam.equalsIgnoreCase(text)) {
					System.out.println(block.getGeometry().getBoundingBox());
					System.out.println("Buenaaaaaaaaaa");
					return block.getGeometry().getBoundingBox();
				}
			}
		}
		return null;
	}
	*/

	private static List<PlayerData> mapTable(TableModel table) {

		Map<String, Object> titles = getTitles(table);
		RowModel titleRowModel = (RowModel) titles.get("rowModel");
		int titleRowIndex = (Integer) titles.get("rowIndex");
		// printRowModel(titles);
		List<PlayerData> players = new ArrayList<>();
		
		System.out.println("Titles: "+titleRowModel.getCells());
		
		for (int i = titleRowIndex + 1; i < table.getRows().size(); i++) {
			RowModel rowModel = table.getRows().get(i);
			// printRowModel(rowModel);
			PlayerData playerData = createPlayerData(rowModel, titleRowModel);
			if (playerData != null)
				players.add(playerData);
		}

		return players;
	}

	private static PlayerData createPlayerData(RowModel rowModel, RowModel titleRowModel) {

		List<String> cells = rowModel.getCells();
		// System.out.println("::: "+titleRowModel.getCells().size());

		PlayerData playerData = new PlayerData();
		
		// printRowModel(rowModel);

		for (int i = 0; i < titleRowModel.getCells().size(); i++) {
			String title = titleRowModel.getCells().get(i);
			// System.out.println("("+i+"): "+title);

			if (isOneTitle(title)) {
				
				String titleFormatted = formatTitle(title);
				// System.out.println(title+": "+i);

				// "n°", "nombres", "t.d.", "documento", "jugo", "a", "az", "r", "g"

				switch (titleFormatted) {
				case "n°": {
					playerData.setNumber(mapNumber(cells.get(i)));
					break;
				}
				case "nombres e-1": {
					playerData.setNames(mapString(cells.get(i)));
					playerData.setTeam(1);
					break;
				}
				case "nombres e-2": {
					playerData.setNames(mapString(cells.get(i)));
					playerData.setTeam(2);
					break;
				}
				case "t.d.": {
					playerData.setTypeDocument(mapString(cells.get(i)));
					break;
				}
				case "documento": {
					playerData.setNumberDocument(mapDocument(cells.get(i)));
					break;
				}
				case "jugo": {
					playerData.setPlayed(mapPlayed(cells.get(i)));
					break;
				}
				case "a": {
					playerData.setYellowCards(mapNumber(cells.get(i)));
					break;
				}
				case "az": {
					playerData.setBlueCards(mapNumber(cells.get(i)));
					break;
				}
				case "r": {
					playerData.setRedCards(mapNumber(cells.get(i)));
					break;
				}
				case "g": {
					playerData.setGoals(mapNumber(cells.get(i)));
					break;
				}
				default:
					System.out.println("NO_TITLE_FOUND");
				}
			}else {
				System.out.println("NO_TITLE: "+title);
			}
		}
		if(debug)
			System.out.println(playerData);
		
		if (!isNullOrEmty(playerData.getTypeDocument()) && !isNullOrEmty(playerData.getNumberDocument())
				&& playerData.getNames().length() > 5 && playerData.getNumberDocument().length() > 5
				)
			return playerData;

		return null;
	}

	private static String mapString(String string) {
		if (string == null)
			return "";
		return string.trim();
	}

	private static String mapDocument(String number) {
		if (number == "")
			return number;

		number = number.toLowerCase();
		number = number.replace("t", "7");
		number = number.replace("o", "0");
		number = number.trim();
		return number;
	}
	
	
	private static boolean isNullOrEmty(String string) {
		return string == null || string.isBlank() || string.isEmpty();
	}

	private static int mapNumber(String number) {
		try {
			number = number.toLowerCase();
			number = number.replace("t", "7");
			number = number.replace("o", "0");
			number = number.trim();
			return Integer.parseInt(number);
		} catch (Exception e) {
			if(debug)
				System.err.println(e.getMessage());
		}
		return 0;
	}

	private static boolean mapPlayed(String played) {
		return true;
	}

	/*
	 * private static int getIndexTitle(String title) { if(title == null ||
	 * title.isBlank() || title.isEmpty()) return -1;
	 * 
	 * title = title.toLowerCase(); title = title.trim(); for (int i = 0; i <
	 * titles.size(); i++) { if(title.equalsIgnoreCase(titles.get(i))) { return i; }
	 * } return -1; }
	 */

	private static Map<String, Object> getTitles(TableModel table) {
		RowModel rm = table.getRows().get(0);
		int score = 0, i = 0, rowIndex = 0;
		for (RowModel rowModel : table.getRows()) {
			// printRowModel(rowModel);
			int st = scoreTitle(rowModel);
			if (st >= score) {
				score = st;
				rm = rowModel;
				rowIndex = i;
			}
			i++;
		}
		Map<String, Object> res = new HashMap<>();
		res.put("rowModel", rm);
		res.put("rowIndex", rowIndex);
		return res;
	}

	private static int scoreTitle(RowModel rowModel) {
		List<String> cells = rowModel.getCells();
		int counts = 0;
		for (int i = 0; i < cells.size(); i++) {
			String cell = cells.get(i);
			if (cell != null) {
				if (isOneTitle(cell)) {
					counts++;
				}
			}
		}
		return counts;
	}

	private static String formatTitle(String title) {
		title = title.toLowerCase();
		return title.trim();
	}

	private static boolean isOneTitle(String text) {
		if (text == null || text.isBlank() || text.isEmpty())
			return false;
		
		return titles.contains(formatTitle(text));
	}

	private static TableModel generateTable(Block table, Map<String, Block> blockMap) {
		TableModel model = new TableModel();
		Map<Integer, Map<Integer, String>> rows = getRowsColumnsMap(table, blockMap);

		for (Entry<Integer, Map<Integer, String>> entry : rows.entrySet()) {
			// System.out.println(entry+"\n\n");
			RowModel rowModel = new RowModel();

			// System.out.println("value: "+value);
			// for (Entry<Integer, Map<Integer, String>> entry2 : value)
			/*
			 * for (int i = 1; i <= value.size(); i++) {
			 * System.out.println("("+i+")===> "+value.get(i));
			 * rowModel.getCells().add(value.get(i)); }
			 */

			for (Map.Entry<Integer, String> cell : entry.getValue().entrySet())
				rowModel.getCells().add(cell.getValue());

			model.getRows().add(rowModel);
		}

		return model;
	}
	/*

	private static void printRowModel(RowModel rowModel) {
		if (debug) {
			System.out.println("\n");
			for (int i = 0; i < rowModel.getCells().size(); i++)
				System.out.print("(" + i + ")" + rowModel.getCells().get(i) + " ");
		}
	}
	
	*/

	/*
	private static void printBlock(Block block) {
		if (block != null)
			System.out.println(block.getText() + " - " + block.getBlockType() + " - " + block.getConfidence() + "% - "
					+ block.getTextType() + "\n");
	}
	*/

	private static Map<Integer, Map<Integer, String>> getRowsColumnsMap(Block block, Map<String, Block> blockMap) {

		Map<Integer, Map<Integer, String>> rows = new HashMap<>();

		for (Relationship relationship : block.getRelationships()) {
			if (relationship.getType().equals(RelationshipType.CHILD.toString())) {
				for (String childId : relationship.getIds()) {
					Block cell = blockMap.get(childId);
					if (cell != null) {
						int rowIndex = cell.getRowIndex();
						int colIndex = cell.getColumnIndex();
						if (rows.get(rowIndex) == null) {
							Map<Integer, String> row = new HashMap<>();
							rows.put(rowIndex, row);
						}
						rows.get(rowIndex).put(colIndex, getText(cell, blockMap));
					}
				}
			}
		}
		return rows;
	}

	public static String getText(Block block, Map<String, Block> blockMap) {
		String text = "";
		if (block.getRelationships() != null && block.getRelationships().size() > 0) {
			for (Relationship relationship : block.getRelationships()) {
				if (relationship.getType().equals(RelationshipType.CHILD.toString())) {
					for (String childId : relationship.getIds()) {
						Block wordBlock = blockMap.get(childId);
						if (wordBlock != null && wordBlock.getBlockType() != null) {
							if (wordBlock.getBlockType().equals(BlockType.WORD.toString())) {
								text += wordBlock.getText() + " ";
							}
						}
					}
				}
			}
		}

		return text;
	}
}
