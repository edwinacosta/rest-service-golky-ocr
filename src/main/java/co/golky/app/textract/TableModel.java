package co.golky.app.textract;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class TableModel {

	private List<RowModel> rows;
	
	public TableModel() {
		this.rows = new ArrayList<>();
	}
	
	
}
