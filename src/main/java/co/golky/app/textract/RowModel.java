package co.golky.app.textract;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class RowModel {
	
	private List<String> cells;
	
	public RowModel() {
		this.cells = new ArrayList<>();
	}

}
