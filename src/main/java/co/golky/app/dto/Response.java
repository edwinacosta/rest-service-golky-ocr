package co.golky.app.dto;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class Response {
	
	private boolean status;
	private String message;
	private Object data;
	
	@JsonIgnore
	@SuppressWarnings("unchecked")
	public Map<String, Object> getDataAsMap() {
		return (Map<String, Object>) this.data;
	}
	
	@JsonIgnore
	public JSONObject getDataAsJsonObject() {
		return new JSONObject(this.getDataAsMap());
	}
	
	@JsonIgnore
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getDataAsList() {
		return (List<Map<String, Object>>) this.data;
	}
	
	@JsonIgnore
	public JSONArray getDataAsJsonArray() {
		return new JSONArray(this.getDataAsList());
	}

	@JsonIgnore
	public static Response empty() {
		return Response.builder().message("NOT_FOUND").build();
	}
	
	@JsonIgnore
	public static Response empty(String object) {
		return Response.builder().message(object+"_NOT_FOUND").build();
	}
	
	@JsonIgnore
	public static Response success() {
		return Response.builder().status(true).build();
	}
	
	@JsonIgnore
	public static Response success(String msj) {
		return Response.builder().status(true).message(msj).build();
	}
	
	@JsonIgnore
	public static Response success(String msj, Object data) {
		return Response.builder().status(true).message(msj).data(data).build();
	}
	
	@JsonIgnore
	public static Response error(String msj) {
		return Response.builder().message(msj).build();
	}
	
	@JsonIgnore
	public static Response error(String msj, Object data) {
		return Response.builder().message(msj).data(data).build();
	}
}


