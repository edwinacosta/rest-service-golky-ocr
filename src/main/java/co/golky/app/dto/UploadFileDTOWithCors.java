package co.golky.app.dto;

import org.springframework.http.codec.multipart.FilePart;

import lombok.Data;

@Data
public class UploadFileDTOWithCors {
	
	private FilePart file;
	private Integer table1X;
	private Integer table1Y;
	private Integer table1Width;
	private Integer table1Height;
	
	private Integer table2X;
	private Integer table2Y;
	private Integer table2Width;
	private Integer table2Height;
	

}
