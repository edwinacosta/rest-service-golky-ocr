package co.golky.app.dto;

import org.springframework.http.codec.multipart.FilePart;

import lombok.Data;

@Data
public class UploadFileDTO {
	
	private FilePart file;

}
