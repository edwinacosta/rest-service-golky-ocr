package co.golky.app.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PlayerData {

	private int team;
	private int number;
	private String names;
	private String typeDocument;
	private String numberDocument;
	private boolean played;
	private int yellowCards;
	private int blueCards;
	private int redCards;
	private int goals;
	private int penalties;
}
